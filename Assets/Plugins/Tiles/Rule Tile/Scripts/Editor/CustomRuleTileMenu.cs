﻿using System.Reflection;

namespace UnityEditor
{
    internal static class CustomRuleTileMenu
    {
        [MenuItem("Assets/Create/Custom Rule Tile Script", false, 89)]
        private static void CreateCustomRuleTile()
        {
            CreateScriptAsset("Assets/Tilemap/Tiles/Rule Tile/ScriptTemplates/NewCustomRuleTile.cs.txt",
                "NewCustomRuleTile.cs");
        }

        private static void CreateScriptAsset(string templatePath, string destName)
        {
            typeof(ProjectWindowUtil)
                .GetMethod("CreateScriptAsset", BindingFlags.Static | BindingFlags.NonPublic)
                .Invoke(null, new object[] {templatePath, destName});
        }
    }
}