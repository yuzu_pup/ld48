﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Hints : MonoBehaviour
{

    public event Action<string> NewHintRecieved;
    private bool showing = false;
    public void ShowHint(string name)
    {
        string response = "[e] Interact: ";

        switch (name)
        {
            case "Deliver Order":
                response += name;
                break;
            case "Up":
            case "Down":
                response += ("go "+ name+" a floor.");
                break;
            case "MissingIngredients":
                response = "Sorry, You are missing the required Ingredients!";
                break;
            default:
                response += "pick up " + name;
                break;
        }
        NewHintRecieved?.Invoke(response);
        

    }
    public void HideHint()
    {
        NewHintRecieved?.Invoke("");
    }
}
