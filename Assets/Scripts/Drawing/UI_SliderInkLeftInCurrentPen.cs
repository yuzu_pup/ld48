﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class UI_SliderInkLeftInCurrentPen : MonoBehaviour
{
    public Slider Slider;

    private void Reset()
    {
        Slider = GetComponent<Slider>();
    }

    void Update()
    {
        Slider.value = Singletons.Pens.PercentInkLeftInCurrentPen;
    }
}
