﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Napkins : MonoBehaviour
{
    public Vector2Int TextureSize;
    public int StartingNumberOfNapkins = 1;
    
    public Texture2D CurrentNapkinTexture => _NapkinTextures[_CurrentNapkinIndex];
    
    private List<Texture2D> _NapkinTextures = new List<Texture2D>();
    private int _CurrentNapkinIndex;
    
    private void Start()
    {
        Singletons.GameInput.GoToNextMap += () =>
        {
            _CurrentNapkinIndex = (_CurrentNapkinIndex + 1) % _NapkinTextures.Count;
        };
        
        Singletons.GameInput.GoToPreviousMap += () =>
        {
            _CurrentNapkinIndex = _CurrentNapkinIndex - 1 < 0 ? _NapkinTextures.Count - 1 : _CurrentNapkinIndex - 1;
        };
    }

    public void PickedUpNapkin()
    {
        _NapkinTextures.Add(CreateNewNapkinTexture());
    }
    
    public void StartNewGame()
    {
        _CurrentNapkinIndex = 0;
        
        _NapkinTextures.Clear();
        for (int napkinNumber = 0; napkinNumber < StartingNumberOfNapkins; napkinNumber++)
        {
            _NapkinTextures.Add(CreateNewNapkinTexture());
        }
    }
    
    private Texture2D CreateNewNapkinTexture()
    {
        Texture2D napkin = new Texture2D(TextureSize.x, TextureSize.y, TextureFormat.RGBA32, false);
        napkin.filterMode = FilterMode.Point;
        for (int x = 0; x < TextureSize.x; x++)
        {
            for (int y = 0; y < TextureSize.y; y++)
            {
                napkin.SetPixel(x, y, new Color(0,0,0,0));
            }
        }

        napkin.Apply();
        return napkin;
    }
}
