﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Drawing : MonoBehaviour
{
    public GameObject NapkinRoot;
    public bool _Active = false;

    void Start()
    {
        Singletons.GameInput.OpenMap += Toggle;
        Singletons.GameInput.CloseMap += Toggle;
    }
    
    public void Toggle()
    {
        _Active = !_Active;
        NapkinRoot.SetActive(_Active);
        if (_Active)
        {
            Singletons.Sounds.PlayOpenMapSoundOneShot();

        }
        else
        {
            Singletons.Sounds.PlayCloseMapSoundOneShot();
        }
    }
}
