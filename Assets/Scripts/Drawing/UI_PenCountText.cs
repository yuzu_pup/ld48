﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UI_PenCountText : MonoBehaviour
{
    public TextMeshProUGUI Text;
    private void Reset()
    {
        Text = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        Text.text = Singletons.Pens.NumberOfPensInReserve.ToString();
    }
}
