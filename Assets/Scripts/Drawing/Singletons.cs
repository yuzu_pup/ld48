﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(GameInput))]
[RequireComponent(typeof(Pens))]
[RequireComponent(typeof(Napkins))]
[RequireComponent(typeof(Timers))]
[RequireComponent(typeof(Inventory))]
[RequireComponent(typeof(WorldGen))]
[RequireComponent(typeof(FloorManager))]
[RequireComponent(typeof(Orders))]
[RequireComponent(typeof(Sounds))]
[RequireComponent(typeof(Hints))]
public class Singletons : MonoBehaviour
{
    private const string _ResourcesPathToSingletonsPrefab = "Prefabs/Singletons";
    private static Singletons _SingletonsBackingVar;
    private static Singletons _Instance
    {
        get
        {
            if (_SingletonsBackingVar == null)
            {
                GameObject singletonsInst = (GameObject) Instantiate(Resources.Load(_ResourcesPathToSingletonsPrefab));
                singletonsInst.name = "Singletons";
                DontDestroyOnLoad(singletonsInst);
                _SingletonsBackingVar = singletonsInst.GetComponent<Singletons>();
            }

            return _SingletonsBackingVar;
        }
    }
    
    private void Awake()
    {
        _SingletonsBackingVar = this;
        StartNewGame();
    }

    private void StartNewGame()
    {
        _Napkins.StartNewGame();
        _Orders.GetNewOrder();
    }


    [SerializeField]
    private GameInput _GameInput;
    [SerializeField]
    private Pens _Pens;
    [SerializeField]
    private Napkins _Napkins;
    [SerializeField] 
    private Timers _Timers;
    [SerializeField] 
    private Inventory _Inventory;
    [SerializeField]
    private Ingredients _Ingredients;
    [SerializeField]
    private GameLighting _GameLighting;
    [SerializeField]
    private FloorManager _FloorManager;
    [SerializeField]
    private Orders _Orders;
    [SerializeField]
    private Sounds _Sounds;
    [SerializeField]
    private Hints _Hints;

    public static GameInput GameInput => _Instance._GameInput;
    public static Pens Pens => _Instance._Pens;
    public static Napkins Napkins => _Instance._Napkins;
    public static Timers Timers => _Instance._Timers;
    public static Inventory Inventory => _Instance._Inventory;
    public static Ingredients Ingredients => _Instance._Ingredients;
    public static GameLighting GameLighting => _Instance._GameLighting;
    public static FloorManager FloorManager => _Instance._FloorManager;
    public static Orders Orders => _Instance._Orders;
    public static Sounds Sounds => _Instance._Sounds;
    public static Hints Hints => _Instance._Hints;

    public static event Action OnWorldChanged;
    public static void WorldChanged() {
        OnWorldChanged?.Invoke();
    }
}
