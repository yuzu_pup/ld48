﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pens : MonoBehaviour
{
    public int StartingNumberOfPens = 1;
    public int PixelsPerPen = (int) (600 * 600 * 0.1f);
    private int PixelsLeftInCurrentPen = 0;

    public int NumberOfPensInReserve { get; private set; }
    public bool HavePensInReserve => NumberOfPensInReserve > 0;
    public bool CurrentPenIsEmpty => PixelsLeftInCurrentPen == 0;
    public bool CanStillDraw => !CurrentPenIsEmpty;

    public float PercentInkLeftInCurrentPen => (float) PixelsLeftInCurrentPen / (float) PixelsPerPen;

    private void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        NumberOfPensInReserve = StartingNumberOfPens;
        SwitchToNewPen();
    }

    public void PickedUpPen()
    {
        NumberOfPensInReserve++;
    }

    public void DrewPixels(int numberOfPixels)
    {
        PixelsLeftInCurrentPen = Mathf.Max(0, PixelsLeftInCurrentPen - numberOfPixels);
    }

    public void SwitchToNewPen()
    {
        NumberOfPensInReserve--;
        PixelsLeftInCurrentPen = PixelsPerPen;
    }
}
