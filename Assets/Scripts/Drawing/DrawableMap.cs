﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawableMap : MonoBehaviour
{
    public RawImage Image;
    public int BrushSize = 3;
    public Color BrushColor;
    public Texture2D TextureToDrawOn => Singletons.Napkins.CurrentNapkinTexture;

    public Vector2Int LastMousePos;
    public Vector2Int CurrentMousePos;

    private bool _StartedDrawingSound = false;

    private bool TryingToDraw;

    private void Start()
    {
        Singletons.GameInput.StartDrawing += () =>
        {
            TryingToDraw = true;
            LastMousePos = GetLocalMouseCoords();
        };
        Singletons.GameInput.StopDrawing += () => { TryingToDraw = false; };
        Singletons.GameInput.ChangePens += ChangePensIfCurrentPenEmpty;
        Image.texture = TextureToDrawOn;
    }

    private void Update()
    {
        Pens pens = Singletons.Pens;

        if (TryingToDraw)
        {
            if (pens.CanStillDraw)
            {
                if (_StartedDrawingSound == false)
                {
                    Singletons.Sounds.StartScribbleLoop();
                    _StartedDrawingSound = true;
                }
                
                CurrentMousePos = GetLocalMouseCoords();

                int pixelsDrawn = 0;
                float mag = (CurrentMousePos - LastMousePos).magnitude;
                
                if (mag > 0)
                {
                    for(int currIteration = 0; currIteration < mag; currIteration += BrushSize / 2 )
                    {
                        Vector2 lerped = Vector2.Lerp(LastMousePos, CurrentMousePos,
                            currIteration / mag);
                        pixelsDrawn += DrawAtLocation((int) lerped.x, (int) lerped.y);
                    }
                }
                else
                {
                    pixelsDrawn += DrawAtLocation(CurrentMousePos.x, CurrentMousePos.y);
                }

                if (pixelsDrawn > 0)
                {
                    TextureToDrawOn.Apply();
                    pens.DrewPixels(pixelsDrawn);
                }

                LastMousePos = CurrentMousePos;
            }
            else
            {
                if (_StartedDrawingSound)
                {
                    Singletons.Sounds.StopScribbleLoop();
                    _StartedDrawingSound = false;
                }
            }
        }
        else
        { 
            if (_StartedDrawingSound)
            {
                Singletons.Sounds.StopScribbleLoop();
                _StartedDrawingSound = false;
            }
        }
        
        
        Image.texture = TextureToDrawOn;
    }

    private void ChangePensIfCurrentPenEmpty()
    {
        Pens pens = Singletons.Pens;
        
        if (pens.CurrentPenIsEmpty && pens.HavePensInReserve)
        {
            pens.SwitchToNewPen();
        }
    }

    private int DrawAtLocation(int x, int y)
    {
        int halfRadius = BrushSize / 2;
        
        int lowerX = x - halfRadius;
        int upperX = x + halfRadius;
        int lowerY = y - halfRadius;
        int upperY = y + halfRadius;

        int pixelsChanged = 0;

        for (int currX = lowerX; currX <= upperX; currX++)
        {
            for (int currY = lowerY; currY <= upperY; currY++)
            {
                bool inBounds = currX >= 0 && currX < TextureToDrawOn.width 
                                && currY >= 0 && currY < TextureToDrawOn.height;
                if (inBounds)
                {
                    Color currentColor = TextureToDrawOn.GetPixel(currX, currY);
                    if (currentColor != BrushColor)
                    {
                        TextureToDrawOn.SetPixel(currX, currY, BrushColor);
                        pixelsChanged++;
                    }
                }
            }
        }
        
        return pixelsChanged;
    }

    private Vector2Int GetLocalMouseCoords()
    {
        Vector2 localPoint = Vector2.zero;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            Image.rectTransform,
            Input.mousePosition,
            null,
            out localPoint
        );
        return new Vector2Int((int) localPoint.x, (int) localPoint.y);
    }
}

