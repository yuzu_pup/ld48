﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ui_Hints : MonoBehaviour
{

    public TextMeshProUGUI hinty;
    // Start is called before the first frame update
    void Start()
    {
        Singletons.Hints.NewHintRecieved += hint => hinty.text = hint;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
