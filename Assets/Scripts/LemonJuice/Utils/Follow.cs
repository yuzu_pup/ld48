﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LemonJuice
{
    public class Follow : MonoBehaviour
    {
        public Transform Target;

        public bool FollowX;
        public bool FollowY;
        public bool FollowZ;

        // Update is called once per frame
        void Update()
        {
            Vector3 t = Target.position;
            Vector3 myOld = transform.position;
            Vector3 followPos = new Vector3
            (
                FollowX ? t.x : myOld.x,
                FollowY ? t.y : myOld.y,
                FollowZ ? t.z : myOld.z
            );
            transform.position = followPos;
        }
    }
}
