﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class ListExtensions
{
    public static bool RemoveFirst<T>(this List<T> list, Predicate<T> searchFunction)
    {
        int firstIndex = list.FindIndex(searchFunction);
        if (firstIndex == -1) return false;
        list.RemoveAt(firstIndex);
        return true;
    }
}

