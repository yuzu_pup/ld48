﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LemonJuice
{
    public class ImageAnimator : SpriteAnimator
    {
        public Image Image;

        public override void SetSprite(Sprite sprite)
        {
            Image.sprite = sprite;
        }

        public override void MirroringChanged(bool mirrorX, bool mirrorY)
        {
            throw new System.NotImplementedException();
        }
    }
}
