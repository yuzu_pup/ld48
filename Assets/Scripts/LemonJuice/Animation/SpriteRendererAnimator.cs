﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LemonJuice
{
    public class SpriteRendererAnimator : SpriteAnimator
{
    public SpriteRenderer Renderer;
    
    public override void SetSprite(Sprite sprite)
    {
        Renderer.sprite = sprite;
    }

    public override void MirroringChanged(bool mirrorX, bool mirrorY)
    {
        Renderer.flipX = mirrorX;
        Renderer.flipY = mirrorY;
    }
}

}
