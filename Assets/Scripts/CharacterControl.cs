﻿using System.Linq;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    public float yAngle;
    public float k = 50f;
    public Transform child;
    public float rotationSpeed = 5f;

    public float walkSpeed = 10f;
    public float runSpeed = 20.0f;

    private Rigidbody2D body;
    private FloorConfig CurrentFloorConfig;
    private float FlashlightBaseIntesity;
    private float AreaLightBaseIntensity;
    public Light LightFlashlight;
    public Light LightAreaLight;
    
    private float horizontal;
    private int LastFloorLevel;

    private Vector2 moveDirection;
    private float moveLimiter = 0.7f;
    private AudioSource myAudioSource;
    private Vector3 playerPos;
    private bool running;
    private bool startedWalkingLoop;
    private float vertical;

    private void Start()
    {
        body = GetComponent<Rigidbody2D>();
        LightFlashlight = child.GetComponent<Light>();
        myAudioSource = GetComponent<AudioSource>();
        Singletons.GameInput.MoveInDirection += MoveVectorUpdated;
        Singletons.GameInput.ToggleLight += ToggleFlashlight;
        Singletons.GameInput.StartRunning += () => { running = true; };
        Singletons.GameInput.StopRunning += () => { running = false; };
        Singletons.FloorManager.FloorChanged += fc =>
        {
            CurrentFloorConfig = fc;
        };

        Singletons.OnWorldChanged += () =>
        {
            if (Singletons.FloorManager.CurrentFloor - LastFloorLevel < 0)
            {
                var downStairs = FindObjectsOfType<LevelChanger>().Where(lc => lc.LevelDelta > 0).ToArray();
                if (downStairs.Length > 0)
                {
                    var pos = downStairs[0].transform.position;
                    transform.position = new Vector3(pos.x, pos.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(0, 0, transform.position.z);
                }
            }
            else
            {
                transform.position = new Vector3(0, 0, transform.position.z);
            }

            LastFloorLevel = Singletons.FloorManager.CurrentFloor;
        };

        Singletons.Sounds.StartRestaurantAmbianceLoop();

        FlashlightBaseIntesity = LightFlashlight.intensity;
        AreaLightBaseIntensity = LightAreaLight.intensity;
        playerPos = transform.position;
    }

    private void Update()
    {
        var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = LightFlashlight.transform.position.z;
        var targetRotation = Quaternion.LookRotation(Vector3.forward, mousePos - LightFlashlight.transform.position);

        LightFlashlight.transform.rotation = targetRotation;
        LightFlashlight.transform.Rotate(-90, 0, 0);

        Singletons.GameLighting.AmbientLightLevel = 1 / (1 + transform.position.sqrMagnitude / k);
        if (CurrentFloorConfig != null)
        {
            LightFlashlight.intensity = CurrentFloorConfig.FlashlightMultiplier * (FlashlightBaseIntesity - Singletons.GameLighting.AmbientLightLevel * 2);
            LightAreaLight.intensity = AreaLightBaseIntensity * CurrentFloorConfig.AreaLightMultiplier;
        }


        if (moveDirection.magnitude > 0)
        {
            if (startedWalkingLoop == false)
            {
                startedWalkingLoop = true;
                Singletons.Sounds.StartWalkLoop(myAudioSource);
            }
        }
        else
        {
            if (startedWalkingLoop)
            {
                startedWalkingLoop = false;
                Singletons.Sounds.StopWalkLoop();
            }
        }

        body.velocity = moveDirection * (running ? runSpeed : walkSpeed);
    }

    private void MoveVectorUpdated(Vector2 direction)
    {
        moveDirection = direction;
    }

    private void ToggleFlashlight()
    {
        LightFlashlight.enabled = !LightFlashlight.enabled;
        Singletons.Sounds.PlayFlashlightClickOneShot();
    }
}