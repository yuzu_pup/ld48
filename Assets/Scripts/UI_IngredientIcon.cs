﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_IngredientIcon : MonoBehaviour
{
    public Image Image;
    
    public void Configure(IngredientDefinition def)
    {
        Image.sprite = def.OverworldIcon;
    }
}
