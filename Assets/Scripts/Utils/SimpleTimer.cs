﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class SimpleTimer : IPoolable
{
    public bool HasExpired { get; private set; }
    
    public float PercentLeft => SecondsLeft / _SecondsSetTo;
    public float SecondsLeft
    {
        get;
        private set;
    }

    private float _SecondsSetTo;

    public event Action Expired;
    public void Clear()
    {
        SecondsLeft = 0;
        _SecondsSetTo = 0;
        HasExpired = false;
    }
    
    public void Set(float time)
    {
        SecondsLeft = time;
        _SecondsSetTo = time;
        HasExpired = false;
    }

    public void ClearListeners()
    {
        Expired = null;
    }

    public void TimeHasElapsed(float secondsElapsed)
    {
        SecondsLeft = Mathf.Max(0, SecondsLeft - secondsElapsed);
        if (SecondsLeft == 0)
        {
            HasExpired = true;
            Expired?.Invoke();
        }
    }

    public void Initialize()
    {
        Clear();
        ClearListeners();
    }
}
