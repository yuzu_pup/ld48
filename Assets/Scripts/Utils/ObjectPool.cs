﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolable
{
    void Initialize();
}

public class ObjectPool<T> where T: class, IPoolable, new() 
{
    private List<T> _Objects;
    private List<bool> _ObjectAtIndexInUse;

    public ObjectPool()
    {
        _Objects = new List<T>();
        _ObjectAtIndexInUse = new List<bool>();
    }
    
    public ObjectPool(int initialCapacity)
    {
        _Objects = new List<T>(initialCapacity);
        _ObjectAtIndexInUse = new List<bool>(initialCapacity);
        
        for (int i = 0; i < initialCapacity; i++)
        {
            GrowPool();
        }
    }
    
    public T Get()
    {
        int freeIndex = _ObjectAtIndexInUse.FindIndex(inUse => !inUse);
        if (freeIndex == -1)
        {
            return GrowPool(true);
        }

        _ObjectAtIndexInUse[freeIndex] = true;
        T freeObject = _Objects[freeIndex];
        freeObject.Initialize();
        return freeObject;
    }

    public void Return(T t)
    {
        int index = _Objects.FindIndex((objInList) => objInList == t);
        if (index == -1)
        {
            Debug.LogError("Tried to return object to pool that was not part of pool?");
        }
        _ObjectAtIndexInUse[index] = false;
    }

    private T GrowPool(bool inUse = false)
    {
        T inst = new T();
        _Objects.Add(inst);
        _ObjectAtIndexInUse.Add(inUse);
        return inst;
    }
}

