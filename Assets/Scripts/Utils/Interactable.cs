﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Interactable : MonoBehaviour
{

    private Func<string> _GetStringToDisplay;
    
    public event Action OnInteract;
    public event Action OnInteractAreaEntered;
    public event Action OnInteractAreaExited;
    
    private bool interactable = false;
    
    private void OnTriggerEnter2D(Collider2D other) {
        interactable = true;
        Debug.Log(_GetStringToDisplay?.Invoke());
        Singletons.Hints.ShowHint(_GetStringToDisplay?.Invoke());
        OnInteractAreaEntered?.Invoke();
        
    }
    private void OnTriggerExit2D(Collider2D other) {
        interactable = false;
        Singletons.Hints.HideHint();
        OnInteractAreaExited?.Invoke();
    }

    public void RegisterToUse(Func<string> functionThatGivesUsAString)
    {
        _GetStringToDisplay = functionThatGivesUsAString;
    }

    public void Start() {
        Singletons.GameInput.Interact += ()=>{
            if (interactable)
            {
                PlayInteractionSound();
                OnInteract();   
            }
        };
    }

    public virtual void PlayInteractionSound()
    {
        Singletons.Sounds.PlayGetItemSoundOneShot();
    }
}
