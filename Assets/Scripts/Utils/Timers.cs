﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timers : MonoBehaviour
{
    private ObjectPool<SimpleTimer> _Pool = new ObjectPool<SimpleTimer>();
    private List<SimpleTimer> _ActiveTimers = new List<SimpleTimer>();
    private List<SimpleTimer> _ExpiredTimers = new List<SimpleTimer>();

    public SimpleTimer GetAndStartTimer(float time)
    {
        SimpleTimer t = _Pool.Get();
        t.Set(time);
        _ActiveTimers.Add(t);
        return t;
    }

    public void Expire(SimpleTimer t)
    {
        _ExpiredTimers.Add(t);
    }
    
    private void CleanUp(SimpleTimer t)
    {
        _ActiveTimers.Remove(t);
        _Pool.Return(t);
    }

    private void Start()
    {
        // NOCOM
        GetAndStartTimer(2f).Expired += () => { Debug.Log("Done!"); };
    }

    private void Update()
    {
        foreach (SimpleTimer t in _ActiveTimers)
        {
            t.TimeHasElapsed(Time.deltaTime);
            if (t.HasExpired)
            {
                _ExpiredTimers.Add(t);
            }
        }

        if (_ExpiredTimers.Count > 0)
        {
            foreach (SimpleTimer t in _ExpiredTimers)
            {
                CleanUp(t);
            }
            _ExpiredTimers.Clear();
        }
    }
}
