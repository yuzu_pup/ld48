﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public Rigidbody2D body;
    public float WalkSpeed = 3;
    public float RunSpeed = 5;
    public Vector2 MoveVector;
    public bool Running;

    void Start()
    {
        Singletons.GameInput.MoveInDirection += moveVec => { MoveVector = moveVec; };
        Singletons.GameInput.StartRunning += () => { Running = true; };
        Singletons.GameInput.StopRunning += () => { Running = false; };
        body = GetComponent<Rigidbody2D>();

        Singletons.OnWorldChanged += () => { body.position = Vector3.zero;};
    }

    void Update()
    {
        body.velocity = (Running ? RunSpeed : WalkSpeed) * MoveVector;
    }
}
