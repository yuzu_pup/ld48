﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using LemonJuice;
using Vector2 = UnityEngine.Vector2;

public class CharactorAnimator : MonoBehaviour
{
    public SpriteRendererAnimator Animator;

    public SpriteAnimationClip Idle_N;
    public SpriteAnimationClip Idle_S;
    public SpriteAnimationClip Idle_E;
    public SpriteAnimationClip Idle_W;
    
    public SpriteAnimationClip Run_N;
    public SpriteAnimationClip Run_S;
    public SpriteAnimationClip Run_E;
    public SpriteAnimationClip Run_W;

    private Vector2 _LastMovementVector;
    private Vector2 _CurrentMovementDirection;
    private Facing _Facing;
    
    public enum Facing
    {
        N,
        S,
        E,
        W
    }

    private void Start()
    {
        Singletons.GameInput.MoveInDirection += vector2 =>
        {
            _LastMovementVector = _CurrentMovementDirection;
            _CurrentMovementDirection = vector2;
            if (_CurrentMovementDirection != _LastMovementVector)
            {
                if (_CurrentMovementDirection.magnitude == 0)
                {
                    PlayAppropriateIdleClip();
                } 
                else
                {
                    FacingChanged();
                    PlayAppropriateRunningClip();
                }
            }
        }; 
    }
    
    public void FacingChanged()
    {
        if (_CurrentMovementDirection.x > 0)
        {
            _Facing = Facing.E;
        }
        else if (_CurrentMovementDirection.x < 0)
        {
            _Facing = Facing.W;
        }
        else if (_CurrentMovementDirection.y > 0)
        {
            _Facing = Facing.N;
        }
        else if (_CurrentMovementDirection.y < 0)
        {
            _Facing = Facing.S;
        }
    }

    private void PlayAppropriateIdleClip()
    {
        SpriteAnimationClip clip;
        switch (_Facing)
        {
            case Facing.N:
                clip = Idle_N;
                break;
            default:
            case Facing.S:
                clip = Idle_S;
                break;
            case Facing.E:
                clip = Idle_E;
                break;
            case Facing.W:
                clip = Idle_W;
                break;
        }
        Animator.Play(clip);
    }

    private void PlayAppropriateRunningClip()
    { 
        SpriteAnimationClip clip;
        switch (_Facing)
        {
            case Facing.N:
                clip = Run_N;
                break;
            default:
            case Facing.S:
                clip = Run_S;
                break;
            case Facing.E:
                clip = Run_E;
                break;
            case Facing.W:
                clip = Run_W;
                break;
        }
        Animator.Play(clip);
    }
    
}

