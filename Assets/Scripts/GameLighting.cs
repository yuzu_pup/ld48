﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameLighting : MonoBehaviour
{
    private float ambientLightLevel;
    public float AmbientLightLevel
    {
        get => ambientLightLevel;
        set {
            
            ambientLightLevel = value;
            AmbientLevelChange?.Invoke(ambientLightLevel);
        }
    }
    public event Action<float> AmbientLevelChange;
    [Range(0f, 1f)]
    public float LightNumberDebug;
    public Color dark;
    public Color light;

    // Start is called before the first frame update
    void Start()
    {
        List<int> a = new List<int>
        {
            1,
            2,
            3,
            4,
            5
        };
        AmbientLevelChange += newLightLevel => RenderSettings.ambientLight = Color.Lerp(dark, light, ambientLightLevel);

        List<int> b = a.Select(AddFive).ToList();
    }

    // Update is called once per frame
    void Update()
    {
        //AmbientLightLevel = LightNumberDebug;
        
    }

    

    int AddFive(int num)
    {
        return (num + 5);
    }
}
