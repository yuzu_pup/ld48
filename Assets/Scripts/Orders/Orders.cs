﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orders : MonoBehaviour
{
    public List<OrderDefinition> PossibleOrders = new List<OrderDefinition>();

    private OrderDefinition _CurrentOrder;

    public OrderDefinition CurrentOrder
    {
        get => _CurrentOrder;
        private set
        {
            _CurrentOrder = value;
            OrderChangedTo?.Invoke(value);
        }
    }

    public event Action<OrderDefinition> OrderChangedTo;
    
    public void GetNewOrder()
    {
        OrderDefinition newOrder = CurrentOrder;
        while (newOrder == CurrentOrder)
        {
            newOrder = PossibleOrders[UnityEngine.Random.Range(0, PossibleOrders.Count)];
        }
        CurrentOrder = newOrder;
    }
    
    public void PlayerMadeCurrentOrder()
    {
        Singletons.Pens.PickedUpPen();
        Singletons.Napkins.PickedUpNapkin();
        GetNewOrder();
    }
}