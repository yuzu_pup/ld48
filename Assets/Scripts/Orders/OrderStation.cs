﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderStation : MonoBehaviour
{
    public GameObject SpeechBubble;
    // Start is called before the first frame update
    void Start()
    {
        Interactable i = GetComponent<Interactable>(); 
        i.OnInteract+=OnInteract;
        i.OnInteractAreaEntered+=OnInteractEnter;
        i.OnInteractAreaExited+=OnInteractExit;
        i.RegisterToUse(() =>
        {
            if (Singletons.Inventory.HasIngredientsNeededFor(Singletons.Orders.CurrentOrder))
            {
                return "Deliver Order";
            }
            else
            {
                return "MissingIngredients";
            }
        });
    }
    void OnInteract()
    {
        Inventory inv = Singletons.Inventory;
        Orders orders = Singletons.Orders;
        
        if(inv.HasIngredientsNeededFor(orders.CurrentOrder)) 
        {
            inv.MakeOrder(orders.CurrentOrder);
            Singletons.Orders.PlayerMadeCurrentOrder();
        }
    }
    void OnInteractEnter() {
        SpeechBubble.SetActive(true);
    }
    void OnInteractExit() {
        SpeechBubble.SetActive(false);
    }
}
