﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Diner/Order Definition")]
public class OrderDefinition : ScriptableObject
{
    public string Name;
    public List<IngredientDefinition> Ingredients = new List<IngredientDefinition>();
}
