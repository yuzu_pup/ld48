﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_OrderSpeechBubble : MonoBehaviour
{
    public GameObject IngredientIconPrefab;
    public Transform IconGrid;
    
    private CharacterControl _CharacterControl;
    
    private List<GameObject> _InstantiatedIcons = new List<GameObject>();

    private void Start()
    {
        Singletons.Orders.OrderChangedTo += o =>
        {
            CleanUp();
            DisplayOrder(o);
        };
    }

    private void OnEnable()
    {
        DisplayOrder(Singletons.Orders.CurrentOrder);
    }

    private void OnDisable()
    {
        CleanUp();
    }

    public void DisplayOrder(OrderDefinition orderDefinition)
    {
        foreach (IngredientDefinition ing in orderDefinition.Ingredients)
        {
            GameObject go = Instantiate(IngredientIconPrefab, IconGrid);
            UI_IngredientIcon icon = go.GetComponent<UI_IngredientIcon>();
            icon.Configure(ing);
            _InstantiatedIcons.Add(go);
        }
    }

    public void CleanUp()
    {
        foreach (GameObject iconGO in _InstantiatedIcons)
        {
            Destroy(iconGO);
        }
        _InstantiatedIcons.Clear();
    }
}
