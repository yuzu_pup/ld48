﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredients : MonoBehaviour
{
    public List<IngredientDefinition> AllIngredients = new List<IngredientDefinition>();
    public IngredientDefinition GetRandomIngredient() => AllIngredients[Random.Range(0, AllIngredients.Count)];
}
