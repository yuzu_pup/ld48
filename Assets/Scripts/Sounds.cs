﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class PlayableSound
{
    public AudioClip Clip;
    [Range(0, 1)]
    public float Volume = 1;
    public bool Loop;

    private AudioSource _LastAssignedSource;
    
    public void PlaySound(AudioSource source)
    {
        _LastAssignedSource = source;
        ConfigureSource(source);
        source.clip = Clip;
        source.Play();
    }

    public void PlayOneShot(AudioSource source)
    {
        source.PlayOneShot(Clip, Volume);
    }
    public void Stop()
    {
        if (_LastAssignedSource != null)
        {
            _LastAssignedSource.Stop();
        }
    }

    private void ConfigureSource(AudioSource source)
    {
        source.volume = Volume;
        source.loop = Loop;
    }
}

public class Sounds : MonoBehaviour
{
    [SerializeField]
    private PlayableSound _RestaurantAmbiance;
    
    [SerializeField]
    private PlayableSound _WalkSound;
    [SerializeField]
    private PlayableSound _OpenMapSound;
    [SerializeField]
    private PlayableSound _CloseMapSound;
    [SerializeField]
    private PlayableSound _GetItemSound;
    [SerializeField]
    private PlayableSound _FanfareSound;
    [SerializeField]
    private PlayableSound _ScribbleSound;
    [SerializeField]
    private PlayableSound _FlashlightClickSound;
    [SerializeField]
    private PlayableSound _MenuSelectedItemSound;
    [SerializeField]
    private PlayableSound _ShopkeeperSound;

    public AudioSource DefaultOneShotSource;
    public AudioSource DefaultLoopingSource;
    public AudioSource DefaultAmbianceSource;

    public AudioMixerGroup DefaultAudioMixerGroup;

    private void Start()
    {
        Singletons.FloorManager.FloorChanged += (fc) =>
        {
            if (fc.AmbianceAudioMixerGroup != null)
            {
                DefaultAmbianceSource.outputAudioMixerGroup = fc.AmbianceAudioMixerGroup;
            }
            else
            {
                DefaultAmbianceSource.outputAudioMixerGroup = DefaultAudioMixerGroup;
            }
            
            if (fc.LoopingAudioMixerGroup != null)
            {
                DefaultOneShotSource.outputAudioMixerGroup = fc.LoopingAudioMixerGroup;
                DefaultLoopingSource.outputAudioMixerGroup = fc.LoopingAudioMixerGroup;
            }
            else
            {
                DefaultOneShotSource.outputAudioMixerGroup = DefaultAudioMixerGroup;
                DefaultLoopingSource.outputAudioMixerGroup = DefaultAudioMixerGroup;
            }
        };
    }

    public void StartRestaurantAmbianceLoop()
    {
        _RestaurantAmbiance.PlaySound(DefaultAmbianceSource);
    }
    
    public void StartWalkLoop(AudioSource playerAudioSource)
    {
        playerAudioSource.outputAudioMixerGroup = DefaultLoopingSource.outputAudioMixerGroup;
        _WalkSound.PlaySound(playerAudioSource);
    }
    
    public void StopWalkLoop()
    {
        _WalkSound.Stop();
    }

    public void StartScribbleLoop()
    {
        _ScribbleSound.PlaySound(DefaultLoopingSource);
    }

    public void StopScribbleLoop()
    {
        _ScribbleSound.Stop();
    }

    public void PlayOpenMapSoundOneShot()
    {
        _OpenMapSound.PlayOneShot(DefaultOneShotSource);
    }
    
    public void PlayCloseMapSoundOneShot()
    {
        _CloseMapSound.PlayOneShot(DefaultOneShotSource);
    }
    
    public void PlayGetItemSoundOneShot()
    {
        _GetItemSound.PlayOneShot(DefaultOneShotSource);
    }
    
    public void PlayFanfareSoundOneShot()
    {
        _FanfareSound.PlayOneShot(DefaultOneShotSource);
    }

    public void PlayFlashlightClickOneShot()
    {
        _FlashlightClickSound.PlayOneShot(DefaultOneShotSource);
    }

    public void PlayMenuSelectedOneShot()
    {
        _MenuSelectedItemSound.PlayOneShot(DefaultOneShotSource);
    }

    public void PlayShopkeeperSoundOneShot()
    {
        _ShopkeeperSound.PlayOneShot(DefaultOneShotSource);
    }
}
