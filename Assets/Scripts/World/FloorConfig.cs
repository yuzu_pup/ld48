﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Diner/Floor Config")]
public class FloorConfig : ScriptableObject
{
   [System.Serializable]
    public class FixedFloorItem {
        public GameObject Object;
        public Vector3 Pos;
    }


    public Tile FloorTile;
    public RuleTile Walls;
    public float NoiseScale;
    public int Width, Height;
    public float EdgeGradient;
    public FixedFloorItem[] FixedFloorItems;
    public GameObject[] ToSpawnRandomly;
    public bool UseExactSeed = true;
    public int Seed;
    public IngredientDefinition[] Ingredients;
    public Color DarkColor;
    public Color LightColor;
    public float AreaLightMultiplier = 1f;
    public float FlashlightMultiplier = 1f;
    public AudioMixerGroup LoopingAudioMixerGroup;
    public AudioMixerGroup AmbianceAudioMixerGroup;

    public bool DEBUG_DO_NOT_USE;
}
