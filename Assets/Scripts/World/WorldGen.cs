﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WorldGen : MonoBehaviour
{
    public GameObject WorldIngredientPrefab;
    private Tilemap map;
    private int UnixTimeAtStart = (int) System.DateTime.Now.Ticks;
    void Start()
    {
        
        map = GameObject.FindObjectOfType<Tilemap>();
        Singletons.FloorManager.FloorChanged += Generate;
    }

    private Dictionary<Vector2Int, GameObject> ItemsByPos;
    private List<GameObject> Spawned = new List<GameObject>();
    // Start is called before the first frame update

    private static int WALL_LAYER = 0;
    private static int FLOOR_LAYER = -1;
    private static int ITEM_LAYER = -2;

    FloorConfig lastConfig;
    void GenerateBaseMap(FloorConfig config, System.Random rng)
    {
        lastConfig = config;

        Vector2Int Offset = new Vector2Int(rng.Next(100000), rng.Next(100000));
        int MinX = config.Width / -2;
        int MaxX = config.Width / 2;
        int MinY = config.Height / -2;
        int MaxY = config.Height / 2;
        float Scale = config.NoiseScale;
        float edgeGradient = config.EdgeGradient;
        Tile floor = config.FloorTile;
        RuleTile tile = config.Walls;

        for (int x = MinX; x <= MaxX; x++)
            for (int y = MinY; y <= MaxY; y++)
            {

                int even_x = (int)(x / 2) * 2;
                int even_y = (int)(y / 2) * 2;
                float v = Mathf.PerlinNoise(
                    even_x * Scale + Offset.x,
                    even_y * Scale + Offset.y
                );

                // Force center of map to have v=0 ( and thus be open)
                v = Mathf.Min(v, new Vector2Int(even_x, even_y).magnitude / 10
                );

                // Force edge of map to have v = 1 (and thus be closed)
                v += Mathf.Pow(new Vector2(even_x / (float)MaxX, even_y / (float)MaxY).magnitude, edgeGradient);

                if (v < .5)
                {
                    map.SetTile(new Vector3Int(x, y, WALL_LAYER), tile);
                    map.SetTile(new Vector3Int(x, y, FLOOR_LAYER), floor);
                }
            }
    }
    void PruneIslands(FloorConfig config)
    {
        HashSet<Vector2Int> reachable = new HashSet<Vector2Int>();
        Queue<Vector2Int> toExplore = new Queue<Vector2Int>();
        toExplore.Enqueue(Vector2Int.zero);
        while (toExplore.Count > 0)
        {
            Vector2Int pos = toExplore.Dequeue();
            if (reachable.Contains(pos)) continue;
            reachable.Add(pos);

            if (inMap(pos + Vector2Int.up)) toExplore.Enqueue(pos + Vector2Int.up);
            if (inMap(pos + Vector2Int.down)) toExplore.Enqueue(pos + Vector2Int.down);
            if (inMap(pos + Vector2Int.left)) toExplore.Enqueue(pos + Vector2Int.left);
            if (inMap(pos + Vector2Int.right)) toExplore.Enqueue(pos + Vector2Int.right);
        }

        int MinX = config.Width / -2;
        int MaxX = config.Width / 2;
        int MinY = config.Height / -2;
        int MaxY = config.Height / 2;
        for (int x = MinX; x <= MaxX; x++)
            for (int y = MinY; y <= MaxY; y++)
            {
                if (reachable.Contains(new Vector2Int(x, y))) continue;
                map.SetTile(new Vector3Int(x, y, WALL_LAYER), null);
                map.SetTile(new Vector3Int(x, y, FLOOR_LAYER), null);
            }

    }
    
    void PlaceItems(FloorConfig config, System.Random rng)
    {
        foreach (GameObject o in config.ToSpawnRandomly)
            PlaceItem(config, o, rng);
        foreach (IngredientDefinition o in config.Ingredients)
            PlaceIngredient(config, rng, o);
        foreach (var o in config.FixedFloorItems)
            Spawned.Add(Instantiate(o.Object, o.Pos, Quaternion.identity));
    }

    void PlaceIngredient(FloorConfig config, System.Random rng, IngredientDefinition ingredientDefinition) {
        GameObject ing = PlaceItem(config, WorldIngredientPrefab, rng);
        ing.GetComponent<WorldIngredient>().SetIngredientDefinition(ingredientDefinition);
    }

    GameObject PlaceItem(FloorConfig config, GameObject o, System.Random rng)
    {
        int MinX = config.Width / -2;
        int MaxX = config.Width / 2;
        int MinY = config.Height / -2;
        int MaxY = config.Height / 2;
        ItemsByPos = new Dictionary<Vector2Int, GameObject>();
        int selfdestruct = 1000;

        Vector2Int pos;
        do
        {
            do
            {
                if (selfdestruct-- == 0) { Debug.Log(":("); return null; }
                int x = rng.Next(MinX, MaxX);
                int y = rng.Next(MinY, MaxY);
                pos = new Vector2Int(x, y);
            } while (!inMap(pos) || isWall(pos));
        } while (ItemsByPos.ContainsKey(pos));


        GameObject obj = Instantiate(o, new Vector3(pos.x, pos.y, ITEM_LAYER), Quaternion.identity);
        Spawned.Add(obj);
        return obj;
    }

    void Generate(FloorConfig floorConfig)
    {
        System.Random rng = new System.Random(floorConfig.UseExactSeed ? floorConfig.Seed : (floorConfig.Seed + UnixTimeAtStart));
        map.ClearAllTiles();
        Spawned.ForEach(GameObject.Destroy);
        GenerateBaseMap(floorConfig, rng);
        PruneIslands(floorConfig);
        PlaceItems(floorConfig, rng);
        Singletons.WorldChanged();
    }

    public bool isWall(Vector2Int pos)
    {
        return map.GetSprite(new Vector3Int(pos.x, pos.y, WALL_LAYER)) != null;
    }
    public bool inMap(Vector2Int pos)
    {
        return map.GetTile(new Vector3Int(pos.x, pos.y, FLOOR_LAYER)) != null;
    }

    void Update()
    {
        if (lastConfig != null && lastConfig.DEBUG_DO_NOT_USE)
        {
            lastConfig.DEBUG_DO_NOT_USE = false;
            Generate(lastConfig);
        }
    }
}
