﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    public FloorConfig[] Floors;
    public int CurrentFloor { get; private set; }
    public event Action<FloorConfig> FloorChanged;

    public void MoveFloor(int levelDelta)
    {
        CurrentFloor += levelDelta;
        Debug.Log("Moved floor to " + CurrentFloor);
        FloorConfig newFloor = Floors[CurrentFloor]; 
        FloorChanged?.Invoke(newFloor);
        Singletons.GameLighting.dark = newFloor.DarkColor;
        Singletons.GameLighting.light = newFloor.LightColor;
    }

    public void Start() {
        MoveFloor(0);
    }
}
