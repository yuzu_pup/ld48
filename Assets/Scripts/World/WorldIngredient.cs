﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Interactable))]
public class WorldIngredient : MonoBehaviour
{
    private IngredientDefinition def;

    void Start()
    {
        Interactable i = GetComponent<Interactable>();
        i.RegisterToUse(() => def.Name);
        i.OnInteract += OnInteract;

    }
    public void SetIngredientDefinition(IngredientDefinition def)
    {
        this.def = def;
        this.GetComponent<SpriteRenderer>().sprite = def.OverworldIcon;
    }

    void OnInteract()
    {
        Singletons.Inventory.AddIngredient(def);
        Destroy(gameObject);

    }
}