﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Interactable))]
public class LevelChanger : MonoBehaviour
{
    public int LevelDelta = 0;

    public void Start()
    {
        Interactable i = GetComponent<Interactable>();
        i.OnInteract += OnInteract;
        i.RegisterToUse(() => LevelDelta < 0 ? "Up" : "Down");
    }

    private void OnInteract() {
        Debug.Log("Level chaner interact");
        Singletons.FloorManager.MoveFloor(LevelDelta);
    }
}
