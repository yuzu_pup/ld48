﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements.StyleEnums;

public class GameInput : MonoBehaviour
{
    public enum InputState
    {
        Overworld,
        Drawing
    }

    private Dictionary<InputState, Action> _StateToHandler;

    public InputState CurrentInputState = InputState.Overworld;

    public event Action<Vector2> MoveInDirection;
    public event Action StartRunning;
    public event Action StopRunning;
    public event Action OpenMap;
    public event Action CloseMap;
    public event Action ToggleLight;
    public event Action Interact;
    
    
    public event Action GoToNextMap;
    public event Action GoToPreviousMap;
    public event Action StartDrawing;
    public event Action StopDrawing;
    public event Action ChangePens;
    

    private void Awake()
    {
        _StateToHandler = new Dictionary<InputState, Action>
        {
            { InputState.Overworld, HandleOverworld },
            { InputState.Drawing, HandleDrawing }
        };
    }

    private void Update()
    {
        _StateToHandler[CurrentInputState]();
    }

    private void HandleOverworld()
    {
        if (Input.GetButtonDown("ToggleMap"))
        {
            OpenMap?.Invoke();
            MoveInDirection?.Invoke(Vector2.zero);
            CurrentInputState = InputState.Drawing;
        }

        if (Input.GetButtonDown("Run"))
        {
            Debug.Log("StartRunning");
            StartRunning?.Invoke();
        }

        if (Input.GetButtonUp("Run"))
        {
            Debug.Log("StopRunning");
            StopRunning?.Invoke();
        }
        if (Input.GetButtonDown("ToggleFlashLight"))
        {
            Debug.Log("ToggleLight");
            ToggleLight?.Invoke();
        }
        if (Input.GetButtonDown("Interact")) {
            Debug.Log("Interact");
            Interact?.Invoke();
        }
        
        Vector2 moveVectorFromAxes = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
        MoveInDirection?.Invoke(moveVectorFromAxes);
    }

    public void HandleDrawing()
    {
        MoveInDirection?.Invoke(Vector2.zero);
        if (Input.GetButtonDown("ToggleMap"))
        {
            CloseMap?.Invoke();
            CurrentInputState = InputState.Overworld;
        }
        
        if (Input.GetButtonDown("Draw"))
        {
            StartDrawing?.Invoke();
        }
        
        if (Input.GetButtonUp("Draw"))
        {
            StopDrawing?.Invoke();
        }

        if (Input.GetButtonDown("ChangePens"))
        {
            ChangePens?.Invoke();
        }
        
        if (Input.GetButtonDown("NextMap"))
        {
            GoToNextMap?.Invoke();
        }
        
        if (Input.GetButtonDown("PreviousMap"))
        {
            GoToPreviousMap?.Invoke();
        }
    }
}
