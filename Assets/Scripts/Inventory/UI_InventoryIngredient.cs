﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_InventoryIngredient : MonoBehaviour
{
    public Image Icon;
    public TextMeshProUGUI Name;
    public Slider FreshnessMeter;
    private InventoryIngredient _ingredient;
    
    public void Configure(InventoryIngredient ingredient)
    {
        _ingredient = ingredient;
        Name.text = ingredient.IngredientDefinition.Name;
        Icon.sprite = ingredient.IngredientDefinition.InventoryIcon;
    }
    
    // Update is called once per frame
    void Update()
    {
        FreshnessMeter.value = _ingredient.FreshnessTimer.PercentLeft;
    }
}
