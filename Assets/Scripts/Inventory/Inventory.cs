﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<InventoryIngredient> Ingredients = new List<InventoryIngredient>();

    public event Action<InventoryIngredient> IngredientAdded;
    public event Action<InventoryIngredient> IngredientRemoved;

    public void AddIngredient(IngredientDefinition ingredient)
    {
        Debug.Log($"Got a: {ingredient.Name}");
        InventoryIngredient invIng = new InventoryIngredient(ingredient);
        Ingredients.Add(invIng);
        IngredientAdded?.Invoke(invIng);
        invIng.FreshnessTimer.Expired += () => RemoveIngredient(invIng);
    }

    public void RemoveIngredient(InventoryIngredient ingredient)
    {
        Ingredients.Remove(ingredient);
        IngredientRemoved?.Invoke(ingredient);
        ingredient.OnDestroy();
    }

    public bool HasIngredientsNeededFor(OrderDefinition order)
    {
        List<IngredientDefinition> need = new List<IngredientDefinition>(order.Ingredients);
        List<IngredientDefinition> have = new List<IngredientDefinition>(Singletons.Inventory.Ingredients.Select(invI => invI.IngredientDefinition));
        
        foreach (IngredientDefinition neededIngredient in need)
        {
            if (have.Remove(neededIngredient) == false)
            {
                return false;
            }
        }

        return true;
    }

    public void MakeOrder(OrderDefinition order)
    {
        if (HasIngredientsNeededFor(order) == false)
        {
            return;
        }
        
        foreach (IngredientDefinition ingDef in order.Ingredients)
        {
            InventoryIngredient invIng = Ingredients.Find((i) => i.IngredientDefinition == ingDef);
            RemoveIngredient(invIng);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            DEBUG_GiveRandomIngredient();
        }
    }

    public void DEBUG_GiveRandomIngredient()
    {
        IngredientDefinition randomDef = Singletons.Ingredients.GetRandomIngredient();
        AddIngredient(randomDef);
    }
}
