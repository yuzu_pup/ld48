﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryIngredient
{
    public IngredientDefinition IngredientDefinition;
    public SimpleTimer FreshnessTimer;
    public bool Fresh => FreshnessTimer.HasExpired == false;

    public InventoryIngredient(IngredientDefinition definition)
    {
        IngredientDefinition = definition;
        FreshnessTimer = Singletons.Timers.GetAndStartTimer(definition.BaseSecondsWillStayFreshFor);
        FreshnessTimer.Expired += () => { Debug.Log($"{definition.Name} went bad"); };
    }

    public void OnDestroy()
    {
        Singletons.Timers.Expire(FreshnessTimer);
    }
}