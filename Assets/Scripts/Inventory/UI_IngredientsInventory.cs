﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_IngredientsInventory : MonoBehaviour
{
    public Transform EntryParent;
    public GameObject EntryPrefab;
    private Dictionary<InventoryIngredient, GameObject> _IngredientDefToUI = new Dictionary<InventoryIngredient, GameObject>();

    private void Start()
    {
        Singletons.Inventory.IngredientAdded += AddNewIngredientUI;
        Singletons.Inventory.IngredientRemoved += RemoveIngredientUIFor;
    }

    public void AddNewIngredientUI(InventoryIngredient ingredient)
    {
        GameObject instance = Instantiate(EntryPrefab);
        UI_InventoryIngredient ui = instance.GetComponent<UI_InventoryIngredient>();
        ui.Configure(ingredient);
        instance.transform.SetParent(EntryParent);
        _IngredientDefToUI.Add(ingredient, instance);
    }

    public void RemoveIngredientUIFor(InventoryIngredient ingredient)
    {
        Destroy(_IngredientDefToUI[ingredient]);
    }
}
