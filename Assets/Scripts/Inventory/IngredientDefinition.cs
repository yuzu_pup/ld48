﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Diner/Ingredient Definition")]
public class IngredientDefinition : ScriptableObject
{
    public string Name;
    public Sprite OverworldIcon;
    public Sprite InventoryIcon;
    public float BaseSecondsWillStayFreshFor;
}
